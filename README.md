# Quran Data
---
# Compatible Font
- Noorehuda

# Repository Structure
```bash
├── Quran.ods            # Spreadsheet
├── build.sh             # Convert Spreadsheet to Json
├── data
│   ├── csv/             # Spreadsheet converted to CSV
│   └── text/            # Quran Suras fetched from https://www.irfan-ul-quran.com/
├── out
│   └── out.json
└── tools/               # Individual scripts
```
# Usage
- Modify Spreadsheet `Quran.ods` **IF REQUIRED**
- run `./build.sh`
- json file will be in `out/`

# Credits
- https://www.irfan-ul-quran.com/
