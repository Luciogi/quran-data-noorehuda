#!/bin/bash
echo "-> Cleaning"
rm  ./data/csv/*
rm -r ./out
mkdir -p ./data/csv
mkdir ./out
cd ./tools
# convert librecalc file to csv
./ods2csv.sh
# rename csv files
./rename-csv.sh
# convert csv files to single json file
echo "-> Writing data to out/out.json"
./convert-csv2json.sh

echo "-> File generated out/out.json <-"
